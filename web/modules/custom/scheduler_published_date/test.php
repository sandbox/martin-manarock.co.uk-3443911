<?php

/**
 * @file
 * Contains test.php.
 */

/**
 * Test function.
 *
 * @return void
 *   Description of the return value.
 */
function test_function() {
  // Indentation error.
  $test = 'This should trigger a coding standard error';
  return $test;
}
